﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public InputField AngleField;
    public Paddle PaddleObject;

	// Use this for initialization
	//void Start () {
		
	//}
	
	// Update is called once per frame
	//void Update () {
		
	//}

    public void SetCurrentAngle()
    {
        if (AngleField.text == "")
        {
            AngleField.text = "0";
        }

        PaddleObject.SetCurrentAngle(float.Parse(AngleField.text));
        PaddleObject.ResetPosition();
    }
}
