﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CObject : MonoBehaviour {

    public CVector pos;
    public CVector vel;
    public float mass;
    public float accel;
    
    // Use this for initialization

    public CObject()
    {
        pos = CVector.CZero();
        mass = 1;
        accel = 0;
        vel = new CVector(2.0f, 5.0f, 0);
    }
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = pos.CVecToVector3();
	}

    public float AddForce(CVector force)
    {
        return Mathf.Atan(force.PosY / force.PosX) * Mathf.Rad2Deg;
    }
}
