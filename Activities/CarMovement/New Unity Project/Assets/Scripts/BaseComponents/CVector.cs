﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CVector {

    // Use this for initialization

    public float PosX;
    public float PosY;
    public float PosZ;
    public float RotDeg;

    public CVector (float x = 0.0f,float y = 0.0f, float z = 1.0f, float deg = 0.0f)
    {
        PosX = x;
        PosY = y;
        PosZ = z;
        RotDeg = deg;
    }

    public void MoveInDirection(float v, float degree)
    {
        PosX += Mathf.Cos(degree * Mathf.Deg2Rad)*v*Time.deltaTime;
        PosY += Mathf.Sin(degree * Mathf.Deg2Rad)*v*Time.deltaTime;
    }

    public float GetAngleInDegrees()
    {
        return Mathf.Atan2(PosY, PosX) * Mathf.Rad2Deg;
    }

    public static CVector CZero()
    {
        return new CVector(0, 0, 1);
    }

    public Vector3 CVecToVector3()
    {
        return new Vector3(PosX, PosY, PosZ);
    }

    public static CVector operator +(CVector a, CVector b)
    {
        return new CVector(a.PosX + b.PosX, a.PosY + b.PosY, a.PosZ + b.PosZ);
    }

    public static CVector operator -(CVector a, CVector b)
    {
        return new CVector(a.PosX - b.PosX, a.PosY - b.PosY, a.PosZ - b.PosZ);
    }

    public static CVector operator *(CVector a, float b)
    {
        return new CVector(a.PosX * b, a.PosY * b, a.PosZ * b);
    }

    public static CVector operator /(CVector a, float b)
    {
        return new CVector(a.PosX / b, a.PosY / b, a.PosZ / b);
    }
}
