﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Paddle : MonoBehaviour {

    // Use this for initialization

    public InputField AngleField;

    private CObject obj;

    private CVector right;

    private float currentAngle;
	void Start () {
        currentAngle = float.Parse(AngleField.text);
        obj = new CObject();
        obj.pos.PosX = transform.position.x;
        obj.pos.PosY = transform.position.y;
        right = new CVector(obj.vel.PosX, 0, 0);

        transform.position = obj.pos.CVecToVector3();
        transform.rotation = Quaternion.AngleAxis(obj.pos.RotDeg, Vector3.forward);
    }
	
	// Update is called once per frame
	void Update () {
        //pos.PosX += Input.GetAxis("Horizontal")/2.0f;
        //pos.PosY += Input.GetAxis("Vertical")/2.0f;
        //pos.MoveInDirection(currentAngle);

        obj.pos += right*Time.deltaTime;
        obj.pos.PosY += Input.GetAxis("Vertical") * Time.deltaTime * obj.vel.PosY;
        //obj.pos.RotDeg = obj.pos.GetAngleInDegrees();
        //currentAngle = obj.pos.RotDeg;
        transform.position = obj.pos.CVecToVector3();
        transform.rotation = Quaternion.AngleAxis(obj.AddForce(new CVector(obj.vel.PosX * -2.0f, Input.GetAxis("Vertical") * obj.vel.PosY, 0))*-1 , Vector3.forward);
    }

    public void SetCurrentAngle(float degree)
    {
        currentAngle = degree;
    }

    public void ResetPosition()
    {
        obj.pos = CVector.CZero();
        transform.position = obj.pos.CVecToVector3();
    }
}
