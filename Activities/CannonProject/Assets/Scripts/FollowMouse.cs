﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour {

    private Camera currentCam;
    private Vector3 mousePos;
    private Vector3 distance;
    private float mouseAngle;
    // Use this for initialization
    void Start () {
        currentCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();		
	}
	
	// Update is called once per frame
	void Update () {
        mousePos = currentCam.ScreenToWorldPoint(Input.mousePosition);
        distance = mousePos - transform.position;
        mouseAngle = Mathf.Atan2(distance.y, distance.x) * Mathf.Rad2Deg;
        
        transform.rotation = Quaternion.AngleAxis(Mathf.Clamp(mouseAngle, 0f, 90f), Vector3.forward);
	}

    public float GetMouseAngle()
    {
        return mouseAngle;
    }

    public Vector3 GetMousePosition()
    {
        return mousePos; 
    }
}
