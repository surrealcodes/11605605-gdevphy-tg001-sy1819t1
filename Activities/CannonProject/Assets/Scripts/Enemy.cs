﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private float intensity;
    private float height;
    private float speed;
    int collideCount;
    // Use this for initialization
    void Start()
    {
        intensity = Random.Range(-20.0f, 20.0f);
        height = Random.Range(10.0f, 100.0f);
        speed = Random.Range(1, 3);
        collideCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-Time.deltaTime * speed, Mathf.Sin(Time.time*intensity)/height, 0);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Cannon"))
        {
            if (collideCount == 0)
            {
                Destroy(collision.gameObject.GetComponentInParent<FollowMouse>());
                collision.gameObject.GetComponent<SpriteRenderer>().color = Color.red;
                
            }
            else if (collideCount > 0)
            {
                collision.gameObject.GetComponentInParent<SpriteRenderer>().color = Color.red;
                Destroy(GameObject.FindGameObjectWithTag("Player"));
                Destroy(collision.gameObject);
            }
            collideCount++;
        }       
    }
}
