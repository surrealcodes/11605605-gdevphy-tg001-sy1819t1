﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserController : MonoBehaviour {

    public FollowMouse Cannon;
    public GameObject BulletPrefab;
    public float Power;

    public Transform SpawnOrigin;
    private Vector3 SpawnPoint;
    private GameObject TempBullet;

    private float mouseAngle;
	// Use this for initialization
	void Start () {
        SpawnPoint = SpawnOrigin.position;
	}
	
	// Update is called once per frame
	void Update () {
        mouseAngle = Cannon.GetMouseAngle();

        if (Input.GetMouseButtonDown(0))
        {
            TempBullet = Instantiate(BulletPrefab, SpawnPoint, Quaternion.identity);
            FireBullet();
        }
	}

    public void FireBullet()
    {
        TempBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Cos(mouseAngle*Mathf.Deg2Rad), Mathf.Sin(mouseAngle*Mathf.Deg2Rad)).normalized*Power;
    }
}
