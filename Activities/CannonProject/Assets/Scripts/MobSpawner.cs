﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawner : MonoBehaviour {

    public GameObject MobPrefab;
    private Camera currentCam;
    private float vertBound;
	// Use this for initialization
	void Start () {
        currentCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        vertBound = -currentCam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;
        StartCoroutine(Spawn());
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator Spawn()
    {
        float rand = 0;
        while (true)
        {
            rand = Random.Range(-vertBound/2.0f, vertBound/2.0f);
            Instantiate(MobPrefab, new Vector3(8, Mathf.Clamp(rand, -4, 4), 0), Quaternion.identity);
            yield return new WaitForSeconds(1.0f);
        }
        
    }
}
