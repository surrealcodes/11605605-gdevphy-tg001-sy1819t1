﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointCounter : MonoBehaviour {

    public string PointsUIPrefix;
    public Text PointsUI;
    public static int currentPoints;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        PointsUI.text = PointsUIPrefix + ": " + currentPoints;
	}
}
