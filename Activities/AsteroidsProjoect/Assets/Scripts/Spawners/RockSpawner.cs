﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// convert to RandomLocationSpawner for reusability
public class RockSpawner : MonoBehaviour {

    public GameObject BigRockPrefab;
    public Camera CurrentCamera;
    public int SpawnLimit;

    private Vector2 SpawnPos;
	// Use this for initialization
	void Start () {
        SpawnPos = new Vector2();
        StartCoroutine(SpawnRock());
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private IEnumerator SpawnRock()
    {
        while (true)
        {
            if (BigRock.RockCount < SpawnLimit)
            {
                SpawnPos = new Vector2(Random.Range(0, Screen.width), Random.Range(0, Screen.height));
                SpawnPos = CurrentCamera.ScreenToWorldPoint(SpawnPos);
                Instantiate(BigRockPrefab, SpawnPos, Quaternion.identity);
            }
            yield return new WaitForSeconds(1.0f);
        }
    }
}
