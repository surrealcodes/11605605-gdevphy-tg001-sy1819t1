﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour {

    #region DECLARATIONS
    // public
    public GameObject BulletPrefab;
    public float SpawnInterval;

    // private
    private bool IsFiring;
    #endregion

    public void Start()
    {
        StartCoroutine(BulletFire());
    }

    private IEnumerator BulletFire()
    {
        while (true)
        {
            if (IsFiring)
            {
                Instantiate(BulletPrefab, transform.position, transform.rotation);

            }
            yield return new WaitForSeconds(SpawnInterval);
        }
    }

    public void SetFireStatus(bool status)
    {
        IsFiring = status;
    }
}
