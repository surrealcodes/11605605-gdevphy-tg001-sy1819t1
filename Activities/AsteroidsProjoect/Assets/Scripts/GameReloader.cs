﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameReloader : MonoBehaviour {
    
    public string SceneName;
    public ShipController CurrentShip;
    
	// Use this for initialization
	void Start () {
        StartCoroutine(ReloadChecker());
	}

    private IEnumerator ReloadChecker()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            if (CurrentShip.isShipDead)
            {
                PointCounter.currentPoints = 0;
                SceneManager.LoadScene(SceneName);
            }
        }
    }
}
