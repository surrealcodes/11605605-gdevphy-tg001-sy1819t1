﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigRock : MonoBehaviour {

    public float TravelSpeed;
    public GameObject SmallRockPrefab;

    private PointValue Reward;
    private float LeftSplit;
    private float RightSplit;
    private bool isDestroyedByBullet;

    private static int InstanceCount;
    public static int RockCount
    {
        get
        {
            return InstanceCount;
        }
    }

    public void Start()
    {
        transform.Rotate(Vector3.forward, Random.Range(-360, 360) * Mathf.Rad2Deg);

        Reward = GetComponent<PointValue>();
        isDestroyedByBullet = false;

        LeftSplit = Random.Range(120, 250);
        RightSplit = -LeftSplit;

        InstanceCount++;
    }

    public void Update()
    {
        transform.Translate(TravelSpeed * Time.deltaTime, 0, transform.position.z);
    }

    public void SplitRock()
    {
        GameObject temp;
        temp = Instantiate(SmallRockPrefab, transform.position, Quaternion.identity);
        temp.transform.Rotate(Vector3.forward, transform.rotation.z * LeftSplit * Mathf.Rad2Deg);

        temp = Instantiate(SmallRockPrefab, transform.position, Quaternion.identity);
        temp.transform.Rotate(Vector3.forward, transform.rotation.z * RightSplit * Mathf.Rad2Deg);

        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        if (isDestroyedByBullet) PointCounter.currentPoints += Reward.PointReward;
        InstanceCount--;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Bullet"))
        {
            isDestroyedByBullet = true;
        }
    }
}