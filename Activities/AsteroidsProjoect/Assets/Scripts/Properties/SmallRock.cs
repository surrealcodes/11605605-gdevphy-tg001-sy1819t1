﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRock : MonoBehaviour {

    public float TravelSpeed;
    private PointValue Reward;
    private bool isDestroyedByBullet;

    void Start()
    {
        Reward = GetComponent<PointValue>();
        isDestroyedByBullet = false;
    }

    // Update is called once per frame
    void Update()
    {
        // automatically moves
        transform.Translate(TravelSpeed * Time.deltaTime, 0, transform.position.z);
    }

    private void OnDestroy()
    {
        if (isDestroyedByBullet) PointCounter.currentPoints += Reward.PointReward;
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Bullet"))
        {
            isDestroyedByBullet = true;
        }
    }
}
