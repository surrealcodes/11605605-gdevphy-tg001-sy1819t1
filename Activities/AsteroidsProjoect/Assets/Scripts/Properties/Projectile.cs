﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float TravelSpeed;
	
	// Update is called once per frame
	void Update () {
        // automatically moves
        transform.Translate(TravelSpeed * Time.deltaTime, 0, transform.position.z);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("BigObstacle"))
        { 
            collision.collider.gameObject.GetComponent<BigRock>().SplitRock();
            Destroy(gameObject);
        }
        if (collision.collider.CompareTag("Obstacle"))
        {
            Destroy(collision.collider.gameObject);
            Destroy(gameObject);
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
