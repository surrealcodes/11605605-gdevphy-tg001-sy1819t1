﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

    #region DECLARATIONS
    // public
    public float MoveRate;
    public float RotateRate;
    public float AccelerationLimit;
    public BulletSpawner BulletSpawnerObj;

    // private
    private float currentDirection;
    private float accelerator;
    private float currentRotateDirection;

    private bool isDead;
    public bool isShipDead
    {
        get
        {
            return isDead;
        }
    }
    #endregion
    
	// Update is called once per frame
	void Update () {
        isDead = false;

        CaptureMoveValues();
        ApplyAcceleration();

        CaptureFireKey();

        transform.Translate(currentDirection * accelerator, 0, transform.position.z);
        transform.Rotate(Vector3.forward, currentRotateDirection);
    }

    #region COLLIDER EVENTS
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Obstacle") || collision.collider.CompareTag("BigObstacle"))
        {
            isDead = true;
            Destroy(gameObject);
        }
    }
    #endregion

    #region SHIP FUNCTIONS 
    public void CaptureFireKey()
    {
        if (Input.GetKey(KeyCode.Z)) BulletSpawnerObj.SetFireStatus(true);
        else BulletSpawnerObj.SetFireStatus(false);
    }

    private void CaptureMoveValues()
    {
        // vertical axis set to x, takes in up and down
        currentDirection = Input.GetAxis("Vertical") * MoveRate * Time.deltaTime;        
        currentRotateDirection = -Input.GetAxis("Horizontal") * RotateRate * Time.deltaTime * Mathf.Rad2Deg;
    }

    // checks for acceleration
    private void ApplyAcceleration()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow))
        {
            if (accelerator < AccelerationLimit)
                accelerator += 0.1f;
        }
        else
        {
            if (accelerator > 0)
            {
                accelerator -= 0.1f;
            }
        }
    }
    #endregion
}
